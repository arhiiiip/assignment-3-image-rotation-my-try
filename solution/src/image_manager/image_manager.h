#ifndef ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_MANAGER_H
#define ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_MANAGER_H

#include "image.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

void free_image(struct image image);

uint8_t get_padding(const uint32_t width);

struct image new_image(size_t width, size_t height);

bool image_rotator(struct image oldImage, struct image newImage);

#endif //ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_MANAGER_H
