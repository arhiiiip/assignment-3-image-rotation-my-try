#include "image_manager.h"

uint8_t get_padding(const uint32_t width) {
    uint8_t padding = (width * sizeof(struct pixel)) % 4;
    if (padding) padding = 4 - padding;
    return padding;
}

struct image new_image(size_t width, size_t height) {
    return (struct image) {
            .width = width,
            .height = height,
            .data = malloc(height * width * sizeof(struct pixel))
    };
}

void free_image(struct image image) {
    free(image.data);
}

bool image_rotator(struct image oldImage, struct image newImage) {
    const size_t new_height = oldImage.width;
    const size_t new_width = oldImage.height;
    newImage = new_image(new_width, new_height);
    for (size_t i = 0; i < new_width; i++) {
        for (size_t j = 0; j < new_height; j++) {
            newImage.data[(i * new_height) + j] = oldImage.data[i + (new_height - 1 - j) * new_width];
        }
    }
    return true;
}
