#ifndef ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_H

#include <stdint.h>
#include <stdio.h>

struct __attribute__((packed)) pixel {
    uint8_t b, g, r;
};

struct image {
    size_t width, height;
    struct pixel *data;
};

#endif //ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_H
