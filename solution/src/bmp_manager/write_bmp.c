#include "write_bmp.h"

const struct bmp_header NEW_TEMPLATE = {
        .bfType = 19778,
        .bfReserved = 0,
        .biSize = 40,
        .biPlanes = 1,
        .biBitCount = 24,
        .biCompression = 0,
        .biXPelsPerMeter = 2835,
        .biYPelsPerMeter = 2835,
        .biClrUsed = 0,
        .biClrImportant = 0
};

struct bmp_header new_header(struct image *image) {
    struct bmp_header new_header = NEW_TEMPLATE;
    new_header.biWidth = image->width;
    new_header.biHeight = image->height;
    new_header.biSizeImage = image->height * image->width * sizeof(struct pixel);
    new_header.bfileSize = new_header.biSizeImage + sizeof(struct bmp_header);
    return new_header;
}


bool write_header(FILE *file, struct bmp_header *header) {
    if (fwrite(header, sizeof(struct bmp_header), 1, file) != 1) {
        return true;
    } else return false;
}

bool write_pixels(FILE *file, struct image image) {
    const uint8_t padding = get_padding(image.width);
    const size_t height = image.height;
    const size_t width = image.width;
    struct pixel *pixels = image.data;
    const uint8_t paddings[3] = {0};
    for (size_t i = 0; i < height; i++) {
        if (fwrite(pixels + i * width, sizeof(struct pixel) * width, 1, file) == 0) return false;
        if ((fwrite(paddings, padding, 1, file) == 0) && padding != 0) return false;
    }
    return true;
}


bool write_bmp(FILE *file, struct image *image) {
    struct bmp_header header = new_header(image);
    if (!write_header(file, &header)) return false;
    *image = new_image(header.biWidth, header.biHeight);
    if (!write_pixels(file, *image)) return false;
    return true;
}


