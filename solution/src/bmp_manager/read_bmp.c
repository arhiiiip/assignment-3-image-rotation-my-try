#include "read_bmp.h"

bool read_header(FILE *file, struct bmp_header *header) {
    if (fread(header, sizeof(struct bmp_header), 1, file) == 1) {
        return true;
    } else return false;
}

bool read_pixels(FILE *file, struct image *image) {
    const uint8_t padding = get_padding(image->width);
    struct pixel *data = image->data;
    const size_t height = image->height;
    const size_t width = image->width;
    for (size_t i = 0; i < height; i++) {
        if (fread(data + width * i, sizeof(struct pixel), width, file) != width) return false;
        if (fseek(file, padding, SEEK_CUR) != 0) return false;
    }
    return true;
}

bool read_bmp(FILE *file, struct image *image) {
    struct bmp_header header = {0};
    if (!read_header(file, &header)) return false;
    *image = new_image(header.biWidth, header.biHeight);
    if (!read_pixels(file, image)) return false;
    return true;
}
