#include "bmp_manager/bmp.h"
#include "bmp_manager/read_bmp.h"
#include "bmp_manager/write_bmp.h"
#include "file_manager/file_manager.h"
#include "image_manager/image.h"
#include "image_manager/image_manager.h"

bool read(FILE *file, const char *filename, struct image image) {
    if (!openfile(file, filename)) return false;
    if (!read_bmp(file, &image)) return false;
    if (!closefile(file)) return false;
    return true;
}

bool write(FILE *file, const char *filename, struct image image) {
    if (!openfile(file, filename)) return false;
    if (!write_bmp(file, &image)) return false;
    if (!closefile(file)) return false;
    return true;
}


int main(int argc, char **argv) {
    if (argc != 3) {
        fprintf(stderr, "%s",
                "Not correct argument. Please, enter 1 arg - file for rotation, and 2 arg - file for rotated image");
        return 0;
    }

    FILE *file = NULL;
    struct image input_image = {0};
    struct image output_image = {0};

    //reading
    if (!read(file, argv[1], input_image)) {
        fprintf(stderr, "%s", "We have trouble with reading");
        free_image(input_image);
        return 0;
    }

    //rotating
    if (!image_rotator(input_image, output_image)) {
        fprintf(stderr, "%s", "We have trouble with rotating");
        free_image(input_image);
        return 0;
    }

    //writing
    if (!write(file, argv[2], output_image)) {
        fprintf(stderr, "%s", "We have trouble with writing");
        free_image(input_image);
        return 0;
    }

    free_image(input_image);
    free_image(output_image);
    return 0;
}
