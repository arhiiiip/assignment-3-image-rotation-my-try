#ifndef ASSIGNMENT_3_IMAGE_ROTATION_FILE_MANAGER_H
#define ASSIGNMENT_3_IMAGE_ROTATION_FILE_MANAGER_H

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

bool openfile(FILE *file, const char *filename);

bool closefile(FILE *file);

#endif //ASSIGNMENT_3_IMAGE_ROTATION_FILE_MANAGER_H
