#include "file_manager.h"

bool openfile(FILE *file, const char *filename) {
    file = fopen(filename, "rb");
    if (file) return true;
    return false;
}

bool closefile(FILE *file) {
    int resultClose = fclose(file);
    if (resultClose) return false;
    return true;
}
